import { Component, OnInit } from '@angular/core';

class Livros{
  titulo: string;
  autor: string;
  resumo: string;
  preco: string;
  categoria: string;
  estado: string
  qtdDePaginas: number;
  isbn: string;
  foto: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  livros: Livros [];
  livro = {
    titulo: 'Harry Potter e o cálice de fogo',
    autor: 'J.K Rowling',
    resumo: 'As aventuras de um jovem bruxo',
    preco: 'R$ 35.90',
    categoria: 'Ficção e Aventura',
    estado: 'Ótimo/Bom',
    qtdDePaginas: 264,
    isbn: "123456789",
    foto: " "
  }
  constructor() { }

  ngOnInit() {
    this.livros = [
      {
        titulo: 'Harry Potter e o cálice de fogo',
        autor: 'J.K Rowling',
        resumo: 'As aventuras de um jovem bruxo',
        preco: 'R$ 35.90',
        categoria: 'Ficção e Aventura',
        estado: 'Ótimo/Bom',
        qtdDePaginas: 264,
        isbn: "123456789",
        foto: " "
      },
      {
        titulo: 'A Revolução dos Bichos',
        autor: 'George Orwell',
        resumo: 'História de animais em uma fazenda',
        preco: 'R$ 19.90',
        categoria: 'Ficção e Aventura',
        estado: 'Ótimo/Bom',
        qtdDePaginas: 152,
        isbn: "123456789",
        foto: " "
      },
      {
        titulo: 'O Homem do Castelo Alto',
        autor: 'Philip K. Dick',
        resumo: 'História distópica com elementos de ficção científica e romance.',
        preco: 'R$ 20.90',
        categoria: 'Ficção e Suspense',
        estado: 'Ótimo/Bom',
        qtdDePaginas: 240,
        isbn: "123456789",
        foto: " "
      }

    ]
  }

}
